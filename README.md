# BDS - Java assignment
This project is a template project to train basics of accessing the database from Java language.


## To build&run the project
Enter the following command in the project root directory.
```
./mvnw clean install
```

## To generate the project and external libraries licenses
Enter the following command in the project root directory
```
./mvnw project-info-reports:dependencies
```
