package org.but.feec.db.training.service;

import org.but.feec.db.training.data.dao.Meeting;
import org.but.feec.db.training.data.dao.MeetingPerson;
import org.but.feec.db.training.data.dao.MeetingWithPersonsView;
import org.but.feec.db.training.data.repository.MeetingRepository;
import org.but.feec.db.training.exceptions.EntityNotFoundException;

import java.util.List;
import java.util.Optional;

public class MeetingService {

    private MeetingRepository meetingRepository;

    public MeetingService(MeetingRepository meetingRepository) {
        this.meetingRepository = meetingRepository;
    }

    // TODO: rename
    public MeetingWithPersonsView findById(Long id) {
        Meeting meeting = meetingRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Meeting with id: " + id + " was not found."));

        List<MeetingPerson> meetingPeople = meetingRepository.listMeetingPersons(meeting.getId());

        return createMeetingWithPersonsView(meeting, meetingPeople);
    }

    public List<Meeting> findAll() {
        return  meetingRepository.findAll();
    }

    private MeetingWithPersonsView createMeetingWithPersonsView(Meeting meeting, List<MeetingPerson> meetingPeople) {
        MeetingWithPersonsView view = new MeetingWithPersonsView();

        view.setId(meeting.getId());
        view.setNote(meeting.getNote());
        view.setPlace(meeting.getPlace());
        view.setStartTime(meeting.getStartTime());
        view.setDuration(meeting.getDuration());

        view.setPersons(meetingPeople);

        return view;
    }
}
