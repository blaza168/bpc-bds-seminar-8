package org.but.feec.db.training.data.dao;

import java.util.Date;

public class Meeting {
    Long id;
    String note;
    String place;
    Date startTime;
    Long duration;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Meeting{" +
                "id=" + id +
                ", note='" + note + '\'' +
                ", place='" + place + '\'' +
                ", startTime=" + startTime +
                ", duration=" + duration +
                '}';
    }
}
