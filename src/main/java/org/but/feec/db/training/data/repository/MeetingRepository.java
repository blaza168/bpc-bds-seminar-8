package org.but.feec.db.training.data.repository;

import org.but.feec.db.training.data.dao.Meeting;
import org.but.feec.db.training.data.dao.MeetingPerson;
import org.but.feec.db.training.data.dao.MeetingWithPersonsView;
import org.but.feec.db.training.data.dao.PersonBasicView;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Optional;

/**
 * TASK: Implement all of the methods
 */
public class MeetingRepository {

    private final JdbcTemplate jdbcTemplate;

    public MeetingRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public Optional<Meeting> findById(Long id) {
        List<Meeting> meetings =
                jdbcTemplate.query("SELECT meeting.id_meeting, meeting.note, meeting.place, meeting.start_time, meeting.duration " +
                                "FROM bds.meeting WHERE id_meeting = ?",
                new BeanPropertyRowMapper<>(Meeting.class), id);

        return meetings.size() > 0 ? Optional.of(meetings.get(0)) : Optional.empty();
    }

    public List<Meeting> findAll() {
        return jdbcTemplate.query("SELECT meeting.id_meeting, meeting.note, meeting.place, meeting.start_time, meeting.duration " +
                        "FROM bds.meeting",
                new BeanPropertyRowMapper<>(Meeting.class));
    }

    public List<MeetingPerson> listMeetingPersons(Long meetingId) {
        return jdbcTemplate.query("SELECT person.email, person.first_name, person.surname " +
                        "FROM bds.person_has_meeting " +
                        "INNER JOIN bds.person ON (person_has_meeting.id_person = person.id_person) " +
                        "WHERE person_has_meeting.id_meeting = 1;",
                new BeanPropertyRowMapper<>(MeetingPerson.class));
    }
}
