package org.but.feec.db.training.data.repository;

import org.but.feec.db.training.api.SQLInjectionExampleDto;
import org.but.feec.db.training.config.DataSourceConfig;
import org.but.feec.db.training.exceptions.DataAccessException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQLInjectionExampleRepository {

    /**
     * Return SQLInjectionExample. This example shows that it is possible to return more than you want via injecting malicious query
     *
     * @return all persons
     */
    public List<SQLInjectionExampleDto> findByNameStatement(String name) {
        String sqlResult = "SELECT id, email, password, full_name FROM bds.sql_injection_table1 s WHERE s.full_name = '" + name + "'";
        try (Connection connection = DataSourceConfig.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sqlResult)) {
            List<SQLInjectionExampleDto> sqlInjectionExampleDtos = new ArrayList<>();
            while (resultSet.next()) {
                sqlInjectionExampleDtos.add(mapToSQLInjectionExampleDto(resultSet));
            }
            return sqlInjectionExampleDtos;
        } catch (SQLException e) {
            throw new DataAccessException("Find all users SQL failed.", e);
        }
    }

    public List<SQLInjectionExampleDto> findByNamePreparedStatement(String name) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection
                     .prepareStatement("SELECT id, email, password, full_name FROM bds.sql_injection_table2 s WHERE s.full_name LIKE ?")) {
            preparedStatement.setString(1, name);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                List<SQLInjectionExampleDto> sqlInjectionExampleDtos = new ArrayList<>();
                while (resultSet.next()) {
                    sqlInjectionExampleDtos.add(mapToSQLInjectionExampleDto(resultSet));
                }
                return sqlInjectionExampleDtos;
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find all users SQL failed.", e);
        }
    }

    /**
     * This example shows SQL injection dropping the table that is not working due to the PreparedStatements.
     *
     * @param email user input expecting email value
     * @return
     */
    public SQLInjectionExampleDto findByEmailStatement(String email) {
        String sqlContactResult = "SELECT id, email, password, full_name FROM bds.sql_injection_table1 WHERE email = '" + email + "'";
        try (Connection connection = DataSourceConfig.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sqlContactResult)) {
            if (resultSet.next()) {
                return mapToSQLInjectionExampleDto(resultSet);
            }
        } catch (SQLException e) {
            throw new DataAccessException("findByEmailStatement failed.", e);
        }
        return null;
    }

    /**
     * This example shows SQL injection dropping the table..
     *
     * @param email user input expecting email value
     * @return
     */
    public SQLInjectionExampleDto findByEmailPreparedStatement(String email) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT id, email, password, full_name FROM bds.sql_injection_table2 WHERE email = ?")) {
            preparedStatement.setString(1, email);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToSQLInjectionExampleDto(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("findByEmailPreparedStatement SQL failed.", e);
        }
        return null;
    }

    private SQLInjectionExampleDto mapToSQLInjectionExampleDto(ResultSet resultSet) throws SQLException {
        SQLInjectionExampleDto sqlInjectionExampleDto = new SQLInjectionExampleDto();
        sqlInjectionExampleDto.setId(resultSet.getLong("id"));
        sqlInjectionExampleDto.setEmail(resultSet.getString("email"));
        sqlInjectionExampleDto.setPassword(resultSet.getString("password"));
        sqlInjectionExampleDto.setName(resultSet.getString("full_name"));
        return sqlInjectionExampleDto;
    }

}
