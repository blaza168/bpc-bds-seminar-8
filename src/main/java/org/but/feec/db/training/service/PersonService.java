package org.but.feec.db.training.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.db.training.api.PersonFullDetailViewDto;
import org.but.feec.db.training.data.dao.PersonBasicView;
import org.but.feec.db.training.data.dao.PersonContact;
import org.but.feec.db.training.data.dao.PersonCreateResponseDto;
import org.but.feec.db.training.data.dao.PersonCreateRequestDto;
import org.but.feec.db.training.data.dao.PersonWithAddressView;
import org.but.feec.db.training.data.repository.PersonRepository;
import org.but.feec.db.training.exceptions.DataAccessException;
import org.but.feec.db.training.exceptions.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

public class PersonService {

    private static final Logger logger = LoggerFactory.getLogger(PersonService.class);

    private PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public void logTest() {
        // TODO use logger to log info, error, and debug message
        logger.info("Info log");
        logger.error("Error log");
        logger.debug("Debug log");
    }

    /**
     * @param id identified of a person
     * @return PersonFullDetailViewDto entity
     * @throws EntityNotFoundException when person was not found
     */
    public PersonFullDetailViewDto findById(Long id) {
        PersonWithAddressView personWithAddressView = personRepository.findByIdWithAddress(id)
                .orElseThrow(() -> new EntityNotFoundException("Person with id: " + id + " was not found."));
        List<PersonContact> contacts = personRepository.findPersonContacts(id);
        if (contacts == null || contacts.isEmpty()) {
            throw new EntityNotFoundException("Person with id: " + id + " was not found.");
        }
        return personFullDetailViewDto(personWithAddressView, contacts);
    }

    /**
     * @return Find all persons
     */
    public List<PersonBasicView> findAll() {
        return personRepository.findAll();
    }

    /**
     * Create person record in the database
     *
     * @param personCreateRequestDto object holding the person data
     * @return
     */
    public PersonCreateResponseDto createPerson(PersonCreateRequestDto personCreateRequestDto) {
        // TODO encrypt the password using BCrypt
        String bcryptHashString = BCrypt.withDefaults().hashToString(10, personCreateRequestDto.getPwd());
        personCreateRequestDto.setPwd(bcryptHashString.toCharArray());

        Optional<PersonCreateResponseDto> personCreateResponseDtoOpt = personRepository.createPerson(personCreateRequestDto);
        return personCreateResponseDtoOpt.orElseThrow(() -> new DataAccessException("ID of newly created person not retrieved."));
    }

    /**
     * Also can be simplified using mapping frameworks
     */
    private PersonFullDetailViewDto personFullDetailViewDto(PersonWithAddressView personWithAddressView, List<PersonContact> contacts) {
        PersonFullDetailViewDto personFullDetailViewDto = new PersonFullDetailViewDto();
        personFullDetailViewDto.setAddress(personWithAddressView.getAddress());
        personFullDetailViewDto.setBirthday(personWithAddressView.getBirthday());
        personFullDetailViewDto.setEmail(personWithAddressView.getEmail());
        personFullDetailViewDto.setFirstName(personWithAddressView.getFirstName());
        personFullDetailViewDto.setNickname(personWithAddressView.getNickname());
        personFullDetailViewDto.setSurname(personWithAddressView.getSurname());
        personFullDetailViewDto.setIdPerson(personWithAddressView.getIdPerson());

        personFullDetailViewDto.setContacts(contacts);
        return personFullDetailViewDto;
    }
}
