package org.but.feec.db.training.api;

import org.but.feec.db.training.data.dao.PersonAddress;
import org.but.feec.db.training.data.dao.PersonContact;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class PersonFullDetailViewDto {
    private Long idPerson;
    private LocalDate birthday;
    private String email;
    private String firstName;
    private String nickname;
    private String surname;
    private PersonAddress address;
    private List<PersonContact> contacts = new ArrayList<>();

    public Long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public PersonAddress getAddress() {
        return address;
    }

    public void setAddress(PersonAddress address) {
        this.address = address;
    }

    public List<PersonContact> getContacts() {
        return contacts;
    }

    public void setContacts(List<PersonContact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return "PersonFullDetailViewDto{" +
                "idPerson=" + idPerson +
                ", birthday=" + birthday +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", nickname='" + nickname + '\'' +
                ", surname='" + surname + '\'' +
                ", address=" + address +
                ", contacts=" + contacts +
                '}';
    }
}
