package org.but.feec.db.training.data.mappers;

import org.but.feec.db.training.data.dao.PersonAddress;
import org.but.feec.db.training.data.dao.PersonWithAddressView;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonWithAddressMapper implements RowMapper<PersonWithAddressView> {
    @Override
    public PersonWithAddressView mapRow(ResultSet rs, int rowNum) throws SQLException {
        PersonWithAddressView person = new PersonWithAddressView();
        person.setIdPerson(rs.getLong("id_person"));
        person.setBirthday(rs.getDate("birthday").toLocalDate());
        person.setEmail(rs.getString("email"));
        person.setFirstName(rs.getString("first_name"));
        person.setNickname(rs.getString("nickname"));
        person.setSurname(rs.getString("surname"));

        PersonAddress personAddress = new PersonAddress();
        personAddress.setCity(rs.getString("city"));
        personAddress.setHouseNumber(rs.getString("house_number"));
        personAddress.setStreet(rs.getString("street"));
        personAddress.setIdAddress(rs.getLong("id_address"));

        person.setAddress(personAddress);
        return person;
    }
}
