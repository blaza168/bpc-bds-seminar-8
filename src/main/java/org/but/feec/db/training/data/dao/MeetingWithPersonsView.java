package org.but.feec.db.training.data.dao;

import java.util.Date;
import java.util.List;

public class MeetingWithPersonsView {
    Long id;
    String note;
    String place;

    Date startTime;
    Long duration;

    List<MeetingPerson> persons;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public List<MeetingPerson> getPersons() {
        return persons;
    }

    public void setPersons(List<MeetingPerson> persons) {
        this.persons = persons;
    }

    @Override
    public String toString() {
        return "MeetingWithPersonsView{" +
                "id=" + id +
                ", note='" + note + '\'' +
                ", place='" + place + '\'' +
                ", startTime=" + startTime +
                ", duration=" + duration +
                ", persons=" + persons +
                '}';
    }
}
