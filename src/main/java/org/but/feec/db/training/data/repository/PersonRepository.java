package org.but.feec.db.training.data.repository;

import org.but.feec.db.training.config.DataSourceConfig;
import org.but.feec.db.training.data.dao.*;
import org.but.feec.db.training.exceptions.DataAccessException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PersonRepository {

    /**
     * Returns all the persons in the database.
     *
     * <p>
     * Note: Never do this in the production! Prefer returning all records from the database table in a paginated way (e.g., with the default size set to 20).
     * </p>
     *
     * @return all persons
     */
    public List<PersonBasicView> findAll() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT id_person, birthday, email, first_name, surname, nickname, id_address FROM bds.person");
             ResultSet resultSet = preparedStatement.executeQuery()) {
            List<PersonBasicView> persons = new ArrayList<>();
            while (resultSet.next()) {
                persons.add(mapToPersonBasicView(resultSet));
            }
            return persons;
        } catch (SQLException e) {
            throw new DataAccessException("Find all users SQL failed.", e);
        }
    }

    public Optional<PersonCreateResponseDto> createPerson(PersonCreateRequestDto personCreateRequestDto) {
        String insertPersonSQL = "INSERT INTO bds.person (birthday, email, first_name, nickname, pwd, surname) VALUES (?,?,?,?,?,?)";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {
            // set prepared statement variables
            preparedStatement.setDate(1, Date.valueOf(personCreateRequestDto.getBirthday()));
            preparedStatement.setString(2, personCreateRequestDto.getEmail());
            preparedStatement.setString(3, personCreateRequestDto.getFirstName());
            preparedStatement.setString(4, personCreateRequestDto.getNickname());
            preparedStatement.setString(5, String.valueOf(personCreateRequestDto.getPwd()));
            preparedStatement.setString(6, personCreateRequestDto.getSurname());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DataAccessException("Creating user failed, no rows affected.");
            }

            // get ID from newly inserted record
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    PersonCreateResponseDto personCreateResponseDto = new PersonCreateResponseDto();
                    personCreateResponseDto.setId(generatedKeys.getLong(1));
                    return Optional.of(personCreateResponseDto);
                } else {
                    throw new DataAccessException("Creating user failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating user failed operation on the database failed.");
        }
    }

    /**
     * @param id person identifier
     * @return return PersonWithAddressView object wrapped to the Optional class
     */
    public Optional<PersonWithAddressView> findByIdWithAddress(Long id) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT p.id_person, birthday, email, first_name, surname, nickname, city, street, house_number, a.id_address" +
                             " FROM bds.person p" +
                             " JOIN bds.address a ON p.id_address = a.id_address" +
                             " WHERE p.id_person = ?")
        ) {
            preparedStatement.setLong(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.ofNullable(mapToPersonWithAdressView(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
        return Optional.empty();
    }

    public List<PersonContact> findPersonContacts(Long id) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT contact, title" +
                             " FROM bds.person p" +
                             " JOIN bds.contact c ON p.id_person = c.id_person" +
                             " JOIN bds.contact_type ct ON c.id_contact_type = ct.id_contact_type" +
                             " WHERE p.id_person = ?")
        ) {
            preparedStatement.setLong(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                List<PersonContact> contacts = new ArrayList<>();
                while (resultSet.next()) {
                    PersonContact contact = new PersonContact();
                    contact.setContact(resultSet.getString("contact"));
                    contact.setContactType(resultSet.getString("title"));
                    contacts.add(contact);
                }
                return contacts;
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person with contacts failed.", e);
        }
    }

    /**
     * @param email person email
     * @return return PersonAuthView object returning username (email) and password from the database
     */
    public PersonAuthView findPersonByEmail(String email) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT email, pwd" +
                             " FROM bds.person p" +
                             " WHERE p.email = ?")
        ) {
            preparedStatement.setString(1, email);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToPersonAuth(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
        return null;
    }


    /**
     * <p>
     * Note: In practice reflection or other mapping frameworks can be used (e.g., MapStruct)
     * </p>
     */
    private PersonAuthView mapToPersonAuth(ResultSet rs) throws SQLException {
        PersonAuthView person = new PersonAuthView();
        person.setEmail(rs.getString("email"));
        person.setPassword(rs.getString("pwd"));
        return person;
    }

    private PersonBasicView mapToPersonBasicView(ResultSet rs) throws SQLException {
        PersonBasicView person = new PersonBasicView();
        person.setIdPerson(rs.getLong("id_person"));
        person.setBirthday(rs.getDate("birthday").toLocalDate());
        person.setEmail(rs.getString("email"));
        person.setFirstName(rs.getString("first_name"));
        person.setNickname(rs.getString("nickname"));
        person.setSurname(rs.getString("surname"));
        return person;
    }


    private PersonWithAddressView mapToPersonWithAdressView(ResultSet rs) throws SQLException {
        PersonWithAddressView person = new PersonWithAddressView();
        person.setIdPerson(rs.getLong("id_person"));
        person.setBirthday(rs.getDate("birthday").toLocalDate());
        person.setEmail(rs.getString("email"));
        person.setFirstName(rs.getString("first_name"));
        person.setNickname(rs.getString("nickname"));
        person.setSurname(rs.getString("surname"));

        PersonAddress personAddress = new PersonAddress();
        personAddress.setCity(rs.getString("city"));
        personAddress.setHouseNumber(rs.getString("house_number"));
        personAddress.setStreet(rs.getString("street"));
        personAddress.setIdAddress(rs.getLong("id_address"));

        person.setAddress(personAddress);
        return person;
    }
}
